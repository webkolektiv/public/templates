var page = document.getElementById("page")
var open = document.getElementById("open-menu");
var close = document.getElementById("close-menu");
var sideNav = document.getElementById("side")

window.onresize = function(event) {
  if (window.innerWidth >= 767) {
    sideNav.classList.remove("mobile-menu-hidden")
    page.classList.remove("small-menu-hidden")
  }
}

function menu() {
  if (window.innerWidth < 767) {
    sideNav.classList.toggle("mobile-menu-hidden")
    page.classList.toggle("small-menu-hidden")
  } else {
    page.classList.toggle("menu-hidden")
    close.classList.toggle("hidden")
    open.classList.toggle("hidden")
  }
}

open.addEventListener('click', function() {
  menu();
}, false);

close.addEventListener('click', function() {
  menu();
}, false);

var photoNav = document.getElementById("photo-nav")
var photoMenu = document.getElementById("photo-menu")
var photoInfo = document.getElementById("photo-info");
var photoMain = document.getElementById("photo-main");
var videoNav = document.getElementById("video-nav");
var videoMenu = document.getElementById("video-menu");
var videoInfo = document.getElementById("video-info");
var videoMain = document.getElementById("video-main");
var booksNav = document.getElementById("books-nav");
var booksMenu = document.getElementById("books-menu");
var booksInfo = document.getElementById("books-info");
var booksMain = document.getElementById("books-main");

function navigate(screen) {
  if (screen == "photo") {
    videoInfo.classList.add("hidden")
    videoMain.classList.add("hidden")
    videoNav.classList.remove("active")
    videoMenu.classList.remove("active")
    booksInfo.classList.add("hidden")
    booksMain.classList.add("hidden")
    booksNav.classList.remove("active")
    booksMenu.classList.remove("active")
    photoInfo.classList.remove("hidden")
    photoMain.classList.remove("hidden")
    photoNav.classList.add("active")
    photoMenu.classList.add("active")
  } else if (screen == "video") {
    photoInfo.classList.add("hidden")
    photoMain.classList.add("hidden")
    photoNav.classList.remove("active")
    photoMenu.classList.remove("active")
    booksInfo.classList.add("hidden")
    booksMain.classList.add("hidden")
    booksNav.classList.remove("active")
    booksMenu.classList.remove("active")
    videoInfo.classList.remove("hidden")
    videoMain.classList.remove("hidden")
    videoNav.classList.add("active")
    videoMenu.classList.add("active")
  } else if (screen == "books") {
    photoInfo.classList.add("hidden")
    photoMain.classList.add("hidden")
    photoNav.classList.remove("active")
    photoMenu.classList.remove("active")
    videoInfo.classList.add("hidden")
    videoMain.classList.add("hidden")
    videoNav.classList.remove("active")
    videoMenu.classList.remove("active")
    booksInfo.classList.remove("hidden")
    booksMain.classList.remove("hidden")
    booksNav.classList.add("active")
    booksMenu.classList.add("active")
  } else {

  }
}

photoNav.addEventListener('click', function() {
  navigate("photo")
}, false);
photoMenu.addEventListener('click', function() {
  navigate("photo")
}, false);

videoNav.addEventListener('click', function() {
  navigate("video")
}, false);
videoMenu.addEventListener('click', function() {
  navigate("video")
}, false);

booksNav.addEventListener('click', function() {
  navigate("books")
}, false);
booksMenu.addEventListener('click', function() {
  navigate("books")
}, false);
